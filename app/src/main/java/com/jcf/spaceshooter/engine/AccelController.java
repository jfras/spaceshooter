package com.jcf.spaceshooter.engine;

import java.util.ArrayList;

import android.view.MotionEvent;

import com.jcf.spaceshooter.model.SpaceShuttle;

public class AccelController extends ShuttleController {

	float ster;
	public AccelController(Input input) {
		super(input);
		ster = 0;
	}

	@Override
	public void ControlShuttle(SpaceShuttle shuttle) {
		double tmp = Math.sqrt(input.getAccX()*input.getAccX() +  input.getAccZ()*input.getAccZ());
		double angle =  Math.atan2( input.getAccY(), tmp);

		ster = (float) (ster*0.9 + 0.1*angle*3);
		shuttle.setX(ster);
		
	}

}
