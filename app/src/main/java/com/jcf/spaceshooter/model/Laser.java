package com.jcf.spaceshooter.model;

import com.jcf.spaceshooter.engine.Assets;

import java.util.ArrayList;

public class Laser extends Gun {


	private int amount;
	public Laser(ArrayList<InteractiveSpaceObject> bullets, int swidth, int sheight) {
		super(swidth,sheight,bullets);
		interval = 1500;
		maxLevel = 8;
		amount = 1;
	}

	@Override
	public boolean fire(int x, int y, float rot ) {

		float dx = 10;
		if(!super.fire(x, y, rot))
			return false;

		float v = 3f;
		float shift = (amount -1)*dx/2f;
		for(int i = 0; i< amount; i++)
		{
			
			float asd = (float) (rot / 1.30 * Math.PI + 0.01*(Math.random() - 0.5))/2.0f;
			for(int j = 0; j<height; j+= Assets.laserBeam.getHeight())
				bullets.add(new LaserBeam((int)(x-shift + dx*i + (float)Math.sin(asd)*(j)), (int)(y -(float)Math.cos(asd)*j), asd, (float)Math.sin(asd)*v, -(float)Math.cos(asd)*v, width, height));
		}
		return true;
	}

	public void upgrade() {
		super.upgrade();
		switch(level)
		{
		case 0:
			interval = 1300;
			amount = 1;
			break;
		case 1:
			interval = 1000;
			amount = 1;
			break;
		case 2:
			interval = 1000;
			amount = 2;
			break;
		case 3:
			interval = 1000;
			amount = 2;
			break;
		case 4:
			interval = 1000;
			amount = 2;
			break;	
		case 5:
			interval = 1000;
			amount = 2;
			break;
		case 6:
			interval = 1000;
			amount = 2;
			break;
		case 7:
			interval = 1000;
			amount = 2;
			break;
		case 8:
			interval = 1000;
			amount = 3;
			break;
		}
	}
}
