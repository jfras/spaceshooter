package com.jcf.spaceshooter.model;

import com.jcf.spaceshooter.engine.Assets;

public class LaserBeam extends Bullet {

	int iteration = 0;

	public LaserBeam(int x, int y,float rot, float velx, float vely, int screenWidth,
					 int screenHeight) {
		super(x, y, velx, vely, screenWidth, screenHeight, Assets.laserBeam);
		power = 1000;
		this.rot = rot;
	}

	public boolean colisionDetection(InteractiveSpaceObject object) {

		double w =  (object.getWidth()+realWidth)/2;

		if (	object.getX() < x + w &&
				object.getX() > x - w )
		{
			object.colisionDetected(this);
			colisionDetected(object);
			return true;
		}
		else
		{
			return false;
		}
	}

	/*public boolean update(int time) {

		iteration ++;
		if (iteration >=10)
		{
			return false;
		}

		return super.update(time);
	}*/
}
